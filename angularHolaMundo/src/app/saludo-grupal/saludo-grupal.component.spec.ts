import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaludoGrupalComponent } from './saludo-grupal.component';

describe('SaludoGrupalComponent', () => {
  let component: SaludoGrupalComponent;
  let fixture: ComponentFixture<SaludoGrupalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaludoGrupalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaludoGrupalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
